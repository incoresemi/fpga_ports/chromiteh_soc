// Copyright (c) 2020 InCore Semiconductors Pvt. Ltd. see LICENSE.incore for more details on licensing terms
/*
Author: Neel Gala, neelgala@incoresemi.com
Created on: Friday 01 May 2020 05:22:44 PM IST

*/

`define AXI4ID 1
`define User 0

//---- APB peripherals
`define DebugBase       'h0000_0100
`define DebugEnd        'h0000_010F
`define BootconfigBase  'h0000_0200
`define BootconfigEnd   'h0000_0203
`define ROMBase         'h0001_0000
`define ROMEnd          'h0001_0FFF
`define Uart0Base       'h0001_1300
`define Uart0End        'h0001_13FF
`define GPIOBase        'h0002_0000
`define GPIOEnd         'h0002_00FF
`define SPIClusterBase  'h0002_0100
`define SPIClusterEnd   'h0002_03FF
`define PWMBase			'h0003_0000
`define PWMEnd			`PWMBase + ((`channels + 1) << 4)
`define QSPICfgBase     'h0004_0000
`define QSPICfgEnd      'h0004_00FF

//---- AXI4-Lite peripherals
`define ClintBase       'h0200_0000
`define ClintEnd        'h020B_FFFF
`define PLICBase        'h0C00_0000
`define PLICEnd         'h0FFF_FFFF
`define OCMBase         'h1000_0000
`define OCMEnd          'h1000_7FFF
`define EthBase         'h1000_8000
`define EthEnd          'h1000_BFFF

// -- AXI4 peripherals
`define APBBridgeBase   `DebugBase
`define APBBridgeEnd    `QSPICfgEnd
`define AXI4LBridgeBase `ClintBase
`define AXI4LBridgeEnd  `EthEnd
`define DDRBase         'h8000_0000
`define DDREnd          'h8FFF_FFFF
`define QSPIMemBase     'h9000_0000
`define QSPIMemEnd      'h9FFF_FFFF
`define OpenBase        'hA000_0000
`define OpenEnd         'hFFFF_FFFF

// -- assign slave numbers
// AXI4
`define AXI4_Masters            3
`define AXI4_Slaves             6
`define APBBridge_slave_num     0
`define AXI4LBridge_slave_num   1
`define DDR_slave_num           2
`define Open_slave_num          3
`define AXI4Err_slave_num       4
`define QSPI_mem_slave_num      5

// APB Slave numbers
`define APB_Slaves              9
`define Debug_slave_num         0
`define Rom_slave_num           1
`define Uart0_slave_num         2
`define Gpio_slave_num          3
`define Pwm_slave_num			4
`define SPICluster_slave_num    5
`define Bootconfig_slave_num    6
`define QSPI_cfg_slave_num      7
`define ApbErr_slave_num        8

// AXI4-lite slave numbers
`define AXI4L_Masters           1
`define AXI4L_Slaves            5
`define Clint_slave_num         0
`define Plic_slave_num          1
`define Ocm_slave_num           2
`define Eth_slave_num           3
`define AXI4LErr_slave_num      4


// --- assign master numbers
`define DMem_master_num         0
`define IMem_master_num         1
`define Debug_master_num        2

// --- Cluster Specific Parameters
// ---------------
// - SPI Cluster -
// ---------------

`define SPICluster_Num_Slaves    4
`define SPICluster_Num_Masters   1

`define SPI0_slave_num           0
`define SPI1_slave_num           1
`define SPI2_slave_num           2
`define SPICluster_err_slave_num 3

`define SPI0Base        'h0002_0100
`define SPI0End         'h0002_01FF
`define SPI1Base        'h0002_0200
`define SPI1End         'h0002_02FF
`define SPI2Base        'h0002_0300
`define SPI2End         'h0002_03FF

// --- Peripheral specific parameters

//PLIC +4 to make it divisible by 4
//(16[GPIO] + 1[UART] + 6[PWM] + 3[SPI] + 6[QSPI] + 1[ETH] = 33 + 3[UNUSED] = 36)
`define plic_interrupt_src    36

//GPIO
`define gpio_interrupt_size   16

//UART
`define uart_interrupt_size    1

//PWM
`define pwmwidth              16
`define channels               6
`define comp_out_en            0
`define pwm_interrupt_size     6

//GPIO
`define spi_interrupt_size     3

//QSPI
`define qspi_interrupt_size    6

//ETH
`define eth_interrupt_size     1

//UNUSED
`define unused_interrupt_size  3
