CHANGELOG
=========

This project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[0.13.0] - 2021-12-11
  - bump devices to 2.0.0 for compatibility with new DCBus features.

[0.12.0] - 2021-09-14
  - added Flashing capacity via OpenOCD for ARTY-100T boards.
  - added booting from flash when DDR mode is selected.
  - minor updates to the documentation
  - changed gateway to new module
  - using new linux compatible plic
  - added missing verilog files to the build verilog directory.
  - adding ethernet lite support
  - Connecting Ethernet Interrupt to New Gateway.
  - Doc Updates for SPI and PWM
  - bumped devices
  - Changes of spi_cluster interrupts
  - documentation updates for getting started

[0.11.0] - 2021-03-27
  - Removed ClockDiv.bsv from the common_bsv
  - Upgraded devices to v1.4.2

[0.10.0] - 2021-04-13
  - initial commit. Borrowed setup from ChromiteM
