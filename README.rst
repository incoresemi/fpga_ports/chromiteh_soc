##############
Chromite-H SoC
##############

The Chromite H SoC is amongst InCore’s ``No-Cost Eval SoC on FPGA`` based on the Chromite core 
generator. This SoC is targetted for Linux/OS class applications requiring a 64-bit 
micro controller.

The documentation of the project is built using sphinx.
For HTML version click `here <https://chromiteh_soc.readthedocs.io>`_

** THE CURRENT ISA is RV64IMACSU. This will change RV64IMAFDCSU in the near future **


