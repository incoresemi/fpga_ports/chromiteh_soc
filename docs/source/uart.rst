.. _uart:

##################################################
Universal Asynchronous Receiver/Transmitter (UART)
##################################################

.. include:: <isonum.txt>

This chapter will discuss the operation of the Universale Receiver/Transmitter (UART)
instantiated in this design. 


IP Details and Available Configuration
======================================


:numref:`UART_ip_details` provides details of the source of the IP and and
details of the memory map.

.. tabularcolumns:: |l|C|

.. _UART_ip_details:

.. table:: UART IP details

  ========================================  ==============
  ..                                        **Value**
  ========================================  ==============
  Provider                                  gitlab
  Vendor                                    incoresemi
  Library                                   blocks/devices
  Version                                   1.1.2
  Ip Type                                   memory_mapped
  Numer of Config Registers                 7
  Direct Memory Region                      None
  Configuration Register Alignment (bytes)  4
  ========================================  ==============

:numref:`UART_configuration_details` provides information of the various
parameters of the IP available at design time and their description

.. tabularcolumns:: |l|l|l|

.. _UART_configuration_details:

.. table:: UART IP Configuration Options

  ===============  ========================  ======================================================================================================================================================================================
  Configuration    Options                   Description
  ===============  ========================  ======================================================================================================================================================================================
  Bus interfaces   APB, AXI4L, AXI4          Choice of bus interface protocol supported on this IP
  Base address     Integer                   The base address where the memory map of the configuration register starts
  Bytes reserved   Integer >= 0X4C           The number of bytes reserved for this instance. This can be much larger than the actual bytes required for the configuration registers but cannot be smaller than the value indicated.
  init_baud        Integer > 1 and  < 65535  An integer indicating the baud_addr value at reset time. Limited to 16 bits.
  fifo_depth       Integer > 1               An integer indicating the depth of the RX and TX fifos
  ===============  ========================  ======================================================================================================================================================================================



UART Instance Details
=====================



:numref:`UART_instance_details` shows the values assigned to parameters of this
instance of the IP.

.. tabularcolumns:: |c|C|

.. _UART_instance_details:

.. table:: UART Instance Parameters and Assigned Values

  ====================  ====================
  **Parameter Name**    **Value Assigned**
  ====================  ====================
  Base Address          0X11300
  Bound Address         0X113FF
  Bytes reserved        0XFF
  Bus Interface         APB
  init_baud             0X145
  fifo_depth            0X10
  ====================  ====================


UART Features
=============

The UART module implements the standard UART, which has RX and TX IO pins. The UART also has one 
interrupt line that gets connected to PLIC.

The following parameters can be configured in software:

* Baud rate: Using ``Baud Register``
* Interrupts: Using ``Interrupt Enable Register`` and ``Status Clear Register``
* RX and TX threshold: Using *rx_thld*, *rx_thld_dirn*, *tx_thld* and *tx_thld_dirn* fields in the ``Control Register``
* RX and RX auto threshold-interrupt reset: Using *rx_thld_auto_rst* and *tx_thld_auto_rst* 
  fields in the ``Control Register``
* Stop bits: Using *stop_bits* in the ``Control Register``
* Parity bits: Using *parity_bits* in the ``Control Register``
* Character size: Using *char_size* in the ``Control Register``

The following parameters can be configured in the hardware:

* The depth of transmit and receive FIFOs (``fifo_depth``)

This particular version of UART supports only the following configuration:

* 8-N-1: 8 data bits, no parity and 1 stop-bit

The UART does not have support for hardware flow control or other modem control
signals.


Register Map
============



The register map for the UART control registers is shown in
:numref:`UART_register_map`. 

.. tabularcolumns:: |l|c|c|c|l|

.. _UART_register_map:

.. table:: UART Register Mapping for all configuration registes

  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+
  | Register-Name   | Offset(hex)   |   Size(Bits) | Reset(hex)   | Description                                                                     |
  +=================+===============+==============+==============+=================================================================================+
  | baud            | 0X0           |           16 | 0X145        | Controls the baud-value of the UART.                                            |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+
  | tx_data         | 0X4           |            8 | 0X0          | Holds data that needs to be sent over UART                                      |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+
  | rx_data         | 0X8           |            8 | 0X0          | Hold data that is received over the UART                                        |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+
  | status          | 0XC           |           32 | 0X7          | Holds various status fields                                                     |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+
  | control         | 0X10          |           32 | 0X2880       | Various control fields to manipulate communication parameters                   |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+
  | status_clear    | 0X14          |            6 | 0X0          | Register to clear various status bits                                           |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+
  | interrupt_en    | 0X18          |           12 | 0X0          | Controls which status bit events should raise an interrupt from the UART module |
  +-----------------+---------------+--------------+--------------+---------------------------------------------------------------------------------+

All addresses not mentioned in the above table within ``Base Address`` and
``Bound Address`` are reserved and accesses to those regions will generate a
slave error on the bus interface





The register access attributes for the UART control registers are shown in 
:numref:`UART_register_access_attr`.

.. tabularcolumns:: |l|c|c|c|C|

.. _UART_register_access_attr:

.. table:: UART Register Access Attributes for all configuration Registers

  ===============  =============  ============  ============  ============
  Register-Name    Access Type    Reset Type    Min Access    Max Access
  ===============  =============  ============  ============  ============
  baud             read-write     synchronous   2B            4B
  tx_data          write-only     synchronous   1B            4B
  rx_data          read-only      synchronous   1B            4B
  status           read-only      synchronous   1B            4B
  control          read-write     synchronous   1B            4B
  status_clear     write-only     synchronous   1B            4B
  interrupt_en     read-write     synchronous   1B            4B
  ===============  =============  ============  ============  ============




BAUD Register
=============

This is a 16-bit read-write register which specifies the current 
divisor used by baud rate generation for both receive and transmit. 
The baudrate and the divior are associated by the following formula:

.. math:: baudrate = Fin/(16 * baud)

where F :subscript:`in` is the clock frequency of the controller in `Hz`.

.. bitfield::
    :bits: 16
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 16, "name": "BAUD", "attr":"read-write" }]


TX_DATA Register
================

This is a write-only 8-bit register. Witing to this register enqueues the data in to transmit FIFO, if the FIFO is able to accept new entries. Writes are ignored if the FIFO is full. Reading from this register will generate an to access fault. Once should typically loop on the TX_NOTFULL status bit to be set before writing to this register.

.. bitfield::
    :bits: 8
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 8, "name": "TX_DATA", "attr":"write-only" }]


RX_DATA Register
================

This is a read-only 8-bit register. Reading from this register will dequeue a character from the receive FIFO and returns the value. If the FIFO is empty a value of 0 is returned. Writes to this regiter will cause an access fault.

.. bitfield::
    :bits: 8
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 8, "name": "RX_DATA", "attr":"read-only" }]


STATUS Register
===============

This is a read-only register holding various status bit. The following table provides information of the various bits and their description

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "TX_DONE", "attr":"read-only" },
    {"bits": 1, "name": "TX_NOTFULL", "attr":"read-only" },
    {"bits": 1, "name": "RX_NOTFULL", "attr":"read-only" },
    {"bits": 1, "name": "RX_NOTEMPTY", "attr":"read-only" },
    {"bits": 1, "name": "TX_THLD_HIT", "attr":"read-only" },
    {"bits": 1, "name": "RX_THLD_HIT", "attr":"read-only" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "PARITY_ERROR", "attr":"read-only" },
    {"bits": 1, "name": "OVERRUN_ERROR", "attr":"read-only" },
    {"bits": 1, "name": "FRAME_ERROR", "attr":"read-only" },
    {"bits": 1, "name": "BREAK_ERROR", "attr":"read-only" },
    {"bits": 20, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _status_subfields:

.. table:: status subfeild description

  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name    | Attribute   | Description                                                                                                                                                          |
  +=========+===============+=============+======================================================================================================================================================================+
  | [0:0]   | tx_done       | read-only   | Indicates if the transmission has finished.                                                                                                                          |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [1:1]   | tx_notfull    | read-only   | Indicates if the transmit FIFO is not full.                                                                                                                          |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [2:2]   | rx_notfull    | read-only   | Indicates if the receive FIFO is not full.                                                                                                                           |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [3:3]   | rx_notempty   | read-only   | Indicates if the receive FIFO is not empty.                                                                                                                          |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [4:4]   | tx_thld_hit   | read-only   | Indicates if the number of elements present in the transmit FIFO is above the set threshold (or below, depending on the *tx_thld_dirn* bit in ``control`` register). |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [5:5]   | rx_thld_hit   | read-only   | Indicates if the number of elements present in the receive FIFO is above the set threshold (or below, depending on the *rx_thld_dirn* bit in ``control`` register).  |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [7:6]   | Reserved      | read-write  | Reads will return zeros and writes will have no effect                                                                                                               |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [8:8]   | parity_error  | read-only   | Indicates if there was a mismatch in the parity bit received, and the computed parity of the received data.                                                          |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [9:9]   | overrun_error | read-only   | Indicates if the receive FIFO is full, and a new character is received.                                                                                              |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [10:10] | frame_error   | read-only   | Indicates if the stop bit that is received is 0.                                                                                                                     |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [11:11] | break_error   | read-only   | Indicates if both, the data, and the stop bits received are all zeros.                                                                                               |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [31:12] | Reserved      | read-write  | Reads will return zeros and writes will have no effect                                                                                                               |
  +---------+---------------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------------+





CONTROL Register
================

This is an 8-bit register which allows manipulation of the communication parameters. The layout of this register is as shown below.

.. bitfield::
    :bits: 32
    :lanes: 4
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 2, "name": "STOP_BITS", "attr":"read-write" },
    {"bits": 2, "name": "PARITY", "attr":"read-write" },
    {"bits": 4, "name": "CHARSIZE", "attr":"read-write" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "TX_THLD_AUTO_RST", "attr":"read-write" },
    {"bits": 1, "name": "RX_THLD_AUTO_RST", "attr":"read-write" },
    {"bits": 1, "name": "TX_THLD_DIRN", "attr":"read-write" },
    {"bits": 1, "name": "RX_THLD_DIRN", "attr":"read-write" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 4, "name": "TX_THLD", "attr":"read-write" },
    {"bits": 4, "name": "RX_THLD", "attr":"read-write" },
    {"bits": 8, "name": "Reserved", "attr":"" ,"type": 0}]




.. tabularcolumns:: |l|l|l|l|

.. _control_subfields:

.. table:: control subfeild description

  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | Bits    | Field Name       | Attribute   | Description                                                                                                                                                                                                                     |
  +=========+==================+=============+=================================================================================================================================================================================================================================+
  | [1:0]   | stop_bits        | read-write  | Defines number of stop bits used in communication. The values 0, 1 and 2 indicate one, one and a half, and, two stop bits respectively. A value of 3 is illegal.                                                                |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [3:2]   | parity           | read-write  | Defines the type of parity for transmit data, and the expected parity of the received data. Values 0, 1 and 2 indicate no parity, odd parity and even parity, respectively. A value of 3 is illegal.                            |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [7:4]   | charsize         | read-write  | Number of bits used in communication. Valid values are 8 and 7.                                                                                                                                                                 |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [9:8]   | Reserved         | read-write  | Reads will return zeros and writes will have no effect                                                                                                                                                                          |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [10:10] | tx_thld_auto_rst | read-write  | Indicates if the *tx_thld_hit* bit in ``status`` register should clear automatically once the threshold once the condition for *tx_thld_hit* is no longer true.                                                                 |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [11:11] | rx_thld_auto_rst | read-write  | Indicates if the *rx_thld_hit* bit in ``status`` register should clear automatically once the threshold once the condition for *rx_thld_hit* is no longer true.                                                                 |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [12:12] | tx_thld_dirn     | read-write  | Indicates if the *tx_thld* bit should be set when the number of elements in the transmit FIFO is less than or equal to (when this bit is 0), or, greater than (when this bit is 1) the *tx_thld* field in ``control`` register. |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [13:13] | rx_thld_dirn     | read-write  | Indicates if the *rx_thld* bit should be set when the number of elements in the receive FIFO is less than or equal to (when this bit is 0), or, greater than (when this bit is 1) the *rx_thld* field in ``control`` register.  |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [15:14] | Reserved         | read-write  | Reads will return zeros and writes will have no effect                                                                                                                                                                          |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [19:16] | tx_thld          | read-write  | Indicates the threshold value for transmit FIFO                                                                                                                                                                                 |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [23:20] | rx_thld          | read-write  | Indicates the threshold value for receive FIFO                                                                                                                                                                                  |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
  | [31:24] | Reserved         | read-write  | Reads will return zeros and writes will have no effect                                                                                                                                                                          |
  +---------+------------------+-------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+





STATUS_CLEAR Register
=====================

This is a 6-bit register which allows the users to reset some of the status bits. The layout of this register is as shown below.

.. bitfield::
    :bits: 6
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "PARITY_ERROR", "attr":"write-only" },
    {"bits": 1, "name": "OVERRUN_ERROR", "attr":"write-only" },
    {"bits": 1, "name": "FRAME_ERROR", "attr":"write-only" },
    {"bits": 1, "name": "BREAK_ERROR", "attr":"write-only" },
    {"bits": 1, "name": "TX_THLD", "attr":"write-only" },
    {"bits": 1, "name": "RX_THLD", "attr":"write-only" }]




.. tabularcolumns:: |l|l|l|l|

.. _status_clear_subfields:

.. table:: status_clear subfeild description

  +--------+---------------+-------------+---------------------------------------------------------------------------------------------+
  | Bits   | Field Name    | Attribute   | Description                                                                                 |
  +========+===============+=============+=============================================================================================+
  | [0:0]  | parity_error  | write-only  | Writing a 1 clears the *parity_error* bit in ``status`` register. Writing 0 has no effect.  |
  +--------+---------------+-------------+---------------------------------------------------------------------------------------------+
  | [1:1]  | overrun_error | write-only  | Writing a 1 clears the *overrun_error* bit in ``status`` register. Writing 0 has no effect. |
  +--------+---------------+-------------+---------------------------------------------------------------------------------------------+
  | [2:2]  | frame_error   | write-only  | Writing a 1 clears the *frame_error* bit in ``status`` register. Writing 0 has no effect.   |
  +--------+---------------+-------------+---------------------------------------------------------------------------------------------+
  | [3:3]  | break_error   | write-only  | Writing a 1 clears the *break_error* bit in ``status`` register. Writing 0 has no effect.   |
  +--------+---------------+-------------+---------------------------------------------------------------------------------------------+
  | [4:4]  | tx_thld       | write-only  | Writing a 1 clears the *tx_thld* bit in ``status`` register. Writing 0 has no effect.       |
  +--------+---------------+-------------+---------------------------------------------------------------------------------------------+
  | [5:5]  | rx_thld       | write-only  | Writing a 1 clears the *rx_thld* bit in ``status`` register. Writing 0 has no effect.       |
  +--------+---------------+-------------+---------------------------------------------------------------------------------------------+





INTERRUPT_EN Register
=====================

This is a read-write register that controls which events in the status bits would trigger an interrupt. The mapping of the fields in this register is identical to that of the ``status`` register. An interrupt is raised if (interrupt_en & status)!=0.

.. bitfield::
    :bits: 12
    :lanes: 1
    :fontsize: 10
    :vspace: 50
    :hspace: 1200

    [
    {"bits": 1, "name": "TX_DONE", "attr":"read-write" },
    {"bits": 1, "name": "TX_NOTFULL", "attr":"read-write" },
    {"bits": 1, "name": "RX_NOTFULL", "attr":"read-write" },
    {"bits": 1, "name": "RX_NOTEMPTY", "attr":"read-write" },
    {"bits": 1, "name": "TX_THLD_HIT", "attr":"read-write" },
    {"bits": 1, "name": "RX_THLD_HIT", "attr":"read-write" },
    {"bits": 2, "name": "Reserved", "attr":"" ,"type": 0},
    {"bits": 1, "name": "PARITY_ERROR", "attr":"read-write" },
    {"bits": 1, "name": "OVERRUN_ERROR", "attr":"read-write" },
    {"bits": 1, "name": "FRAME_ERROR", "attr":"read-write" },
    {"bits": 1, "name": "BREAK_ERROR", "attr":"read-write" }]




.. tabularcolumns:: |l|l|l|l|

.. _interrupt_en_subfields:

.. table:: interrupt_en subfeild description

  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | Bits    | Field Name    | Attribute   | Description                                                                              |
  +=========+===============+=============+==========================================================================================+
  | [0:0]   | tx_done       | read-write  | A value of 1 enables interrupt when *tx_done* bit in the ``status`` register is 1.       |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [1:1]   | tx_notfull    | read-write  | A value of 1 enables interrupt when *tx_notfull* bit in the ``status`` register is 1.    |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [2:2]   | rx_notfull    | read-write  | A value of 1 enables interrupt when *rx_notfull* bit in the ``status`` register is 1.    |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [3:3]   | rx_notempty   | read-write  | A value of 1 enables interrupt when *rx_notempty* bit in the ``status`` register is 1.   |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [4:4]   | tx_thld_hit   | read-write  | A value of 1 enables interrupt when *tx_thld_hit* bit in the ``status`` register is 1.   |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [5:5]   | rx_thld_hit   | read-write  | A value of 1 enables interrupt when *rx_thld_hit* bit in the ``status`` register is 1.   |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [7:6]   | Reserved      | read-write  | Reads will return zeros and writes will have no effect                                   |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [8:8]   | parity_error  | read-write  | A value of 1 enables interrupt when *parity_error* bit in the ``status`` register is 1.  |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [9:9]   | overrun_error | read-write  | A value of 1 enables interrupt when *overrun_error* bit in the ``status`` register is 1. |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [10:10] | frame_error   | read-write  | A value of 1 enables interrupt when *frame_error* bit in the ``status`` register is 1.   |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+
  | [11:11] | break_error   | read-write  | A value of 1 enables interrupt when *break_error* bit in the ``status`` register is 1.   |
  +---------+---------------+-------------+------------------------------------------------------------------------------------------+





IO and Sideband Signals
=======================



The following table describes the io-signals generated from this IP that may
directly or indirectly drive certain IO pads.

.. tabularcolumns:: |l|l|l|l|

.. _UART_io_signals:

.. table:: UART generated IO signals

  ===================  ======  ===========  ===================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ===================
  sin                       1  input        TX port of the UART
  sout                      1  output       RX port of the UART
  ===================  ======  ===========  ===================

.. note:: some of these signals may be muxed with other functional IO from different
  ips and users should refer to any pinmux module available on chip





.. tabularcolumns:: |l|l|l|l|

.. _UART_sb_signals:

.. table:: UART generated side-band signals generated

  ===================  ======  ===========  ========================================================================================
  Signal Name (RTL)      Size  Direction    Description
  ===================  ======  ===========  ========================================================================================
  interrupt                 1  output       Signal indicating an interrupt has been raised by the UART. Signal is connected to PLIC.
  ===================  ======  ===========  ========================================================================================

