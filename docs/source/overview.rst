############
Introduction
############

The Chromite H SoC is amongst InCore's *No-Cost Eval SoC on FPGA* based 
on the Chromite core generator. This SoC is targetted for linux/OS
class applications requiring a 64-bit micro controller. 

The components of the Chromite H SoC are compliant with available RISC-V standards.
The user is advised to refer to the RISC-V unprivileged , privileged and debug spec
for further details.

Following is the list of currently supported FPGA boards/targets:

1. `Arty A7 100t <https://www.xilinx.com/products/boards-and-kits/1-w51quh.html>`_
   Amongst the cheapest development boards with sufficient space to port the current design.

   Options to buy from :
     - `Digilent <https://store.digilentinc.com/arty-a7-artix-7-fpga-development-board-for-makers-and-hobbyists/>`_
     - `Amazon <https://www.amazon.in/Digilent-Artix-7-Development-Makers-Hobbyists/dp/B017BOBNEO?tag=googinhydr18418-21>`_

Overview
========

:numref:`chromiteh_soc_diag` shows the overall block diagram of the Chromite H SoC. 
A quick summary of the components of the SoC is available in :numref:`feature_summary`

.. _chromiteh_soc_diag:

.. figure:: ChromiteH.jpg
   :align: center
   :height: 450px

   Block diagram of the Chromite H SoC

.. tabularcolumns:: |l|L|

.. _feature_summary:

.. table:: Feature Summary of the Chromite H SoC

   =============== ================================================================================
   Feature         Description 
   =============== ================================================================================
   RISC-V Core     The SoC uses the a 64 bit version of the Chromite core with Machine and User
                   modes of operation only. It supports RV64IMACU extensions of the RISC-V ISA. It
                   also includes a branch predictor, 16KiB 4 way instruction cache, 16KiB 4 way
                   data cache and physical memory protection. Refer to :ref:`chromite_core` for
                   more details
   PLIC            A Platform Level Interrupt controller connected to 16 peripheral interrupts with
                   upto 3 levels of priority. Refer to :numref:`plic`
   UART0           Universal Asynchronous/Synchronous Receiver/Transmitter for serial communication
                   Refer :numref:`uart` for more details on this IP.
   CLINT           Core Local Interrupt for generating software and timer interrupts. Refer to
                   :numref:`clint` for more details
   GPIO            General Purpose Input/Output controller
   ROM             Boot ROM with initial boot-code
   OCM             BRAM based On Chip Memory. Refer to :ref:`ocm` for more details.
   DDRx            A DDR IP available from the vendor (Xilinx) depending on the target FPGA. 
   SPI             A serial peripheral interface to communicate with varioud off-chip devices
   QSPI            A quad serial peripheral interface to communicate with off-chip serial flash memories
   Debugger        A JTAG based debugger compliant with the RISC-V debug spec v0.14. Refer to
                   :numref:`debug` for more details.
   =============== ================================================================================
