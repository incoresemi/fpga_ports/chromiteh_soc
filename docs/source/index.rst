#####################
Chromite H Soc Manual
#####################

.. include:: <isonum.txt>

Welcome to the ChromiteH SoCs documentation.
The source code of the SoC is available at: https://gitlab.com/incoresemi/fpga_ports/chromitem_soc


.. note::

  **Proprietary Notice**

  Copyright (c) 2020, InCore Semiconductors Pvt. Ltd.

  Information in this document is provided "as is" with faults, if any.

  InCore expressly disclaims all warranties, representations, and conditions of any kind, whether
  express or implied, including, but not limited to, the implied warranties or conditions of
  merchantability, fitness for a particular purpose and non-infringement.

  InCore does not assume any liability rising out of the application or use of any product or circuit,
  and specifically disclaims any and all liability, including without limitation indirect, incidental,
  special, exemplary, or consequential damages.

  InCore reserves the right to make changes without further notice to any products herein.

.. toctree::
  :glob:
  :maxdepth: 2
  :numbered:

  overview
  memory_map
  getting_started
  fpga_pins
  os_ports
  chromite
  modes
  custom_csrs
  pmp
  monitors
  cache
  mmu
  interrupts
  debug
  triggers
  jtag
  bootconfig
  gpio
  clint
  plic
  uart
  pwm
  spi
  qspi
  licensing
