###############
Getting Started
###############

Requirements
============

All the tools required for this guide and their installation steps have been captured
`here <https://gitlab.com/incoresemi/utils/common_utils/-/blob/master/chromiteh_soc/README.md>`_

.. warn:: Please make sure to build those exact versions mentioned in the above guide, to avoid tool-related
  incompatibility issues.

Building the FPGA Target
========================

Perform the following steps to generate the bit-stream

1. Clone the repository:
    
.. code-block:: shell-session

  $ git clone https://gitlab.com/incoresemi/fpga_ports/chromiteh_soc.git 
  $ cd chromiteh_soc

2. Configure the FPGA board/target: you need to edit only the `board_alias` field in the file 
   chromiteh.yaml. This field takes an alias name of the board you are targeting. Following is 
   the list of currently supported fields:

    - arty100t

3. Run the configuration script:

.. code-block:: shell-session

  $ python -m configure.main

4. Generate Verilog: Change `<jobs>` to the number of parallel threads you want to use for the build.

.. code-block:: shell-session

  $ make -j<jobs> generate_verilog

5. Build the mcs file:

.. code-block:: shell-session

  $ make generate_hexfiles ip_build fpga_build generate_mcs

6. Flash the mcs file:

.. code-block:: shell-session

  $ make program_mcs
 
One the MCS file has been flashed you will have to power-off and on the board (either through a
switch or by removing the USB cable). To confirm successfully build, open a serial communication
with the UART via miniterm and check for the following:

.. code-block:: shell-session

  $ sudo miniterm -f direct --eol CRLF /dev/ttyUSB1 115200

  --- Miniterm on /dev/ttyUSB1  115200,8,N,1 ---
  --- Quit: Ctrl+] | Menu: Ctrl+T | Help: Ctrl+T followed by Ctrl+H ---
  
    _____        _____                
   |_   _|      / ____|               
     | |  _ __ | |     ___  _ __ ___  
     | | | '_ \| |    / _ \| '__/ _ \ 
    _| |_| | | | |___| (_) | | |  __/ 
   |_____|_| |_|\_____\___/|_|  \___| 
  
  
              Chromite M SoC
              Version: 0.9.2
    Build Date: 2020-05-30 21:07:54 IST
  Copyright (c) 2020 InCore Semiconductors
    Available under Apache v2.0 License 

Debug Programs
==============

Once you have successfully build the FPGA target you can debug programs on the FPGA via GDB.

The Chromite M SoC repository includes some benchmark and sample programs which can be run/debugged.
These are available in the ``software`` directory.
For the full list of the available programs:

.. code-block:: shell-session

   $ cd software/
   $ make list-programs
   program-list: coremarks dhrystone hello

To compile and debug a program on the FPGA:

.. code-block:: shell-session

   $ make PROGRAM=dhrystone connect_gdb

This basically compiles the program available in the ``software/programs`` folder using riscv-gcc, 
connects open-ocd to the FPGA target to enable debug mode and launches GDB with the elf loaded in 
the memory. You can now use standard GDB commands like: info reg, info all-registers, continue, 
etc to debug and execute the program.

List of quick GDB commands

.. tabularcolumns:: |l|L|

.. table:: Quick list of common GDB commands that can be used for debugging the core. 

   ======================= ===========================================================================
   Command                 Description
   ======================= ===========================================================================
   c/continue              this cause the core to resume execution from the current pc
   i r                     list all integer register and current pc
   info all-registers      list all regsiters and csrs available on the core
   info reg <csr-name>     list the current value of the csr-name
   stepi                   step through a single instruction
   b \*0x80000000          set a breakpoint at address 0x80000000
   i b                     list all breakpoints
   del <num>               delete breakpoint number <num>
   x/x 0x80000000          list the 4 bytes of content at location 0x80000000
   x/10x 0x80000000        list 10 consecutive 4-byte words starting from location 0x80000000
   load <elf>              load the <elf> into the memory
   file <elf>              read the debug symbols of the <elf>
   compare-sections        compare if the loading of and elf was successfull. Will require running file and load commands on the same elf before issuing this command.
   monitor reset halt      Will reset the SoC except the debug module. Has to be followed by monitor gdb_sync and stepi commands for proper effect.
   ======================= ===========================================================================

Flash Program
==============

Once you have successfully build the FPGA target you can flash programs on the FPGA via OpenOCD.

.. code-block:: shell-session

   $ make flash ELF=< absolute path to ELF >

This basically converts the given ELF to binary using ``objcopy`` and flashes
into the SPI interfaced memory. After flashing, on every reset the flash contents
are copied into DDR.

The flash contents will be copied to DDR only on keeping the ``SW1 = 1``,
``SW0 = 0`` of FPGA. This setting is equivalent to selecting ``boot_mode = 2``

Simulating the SoC
==================

The following can be used to simulate non-propreitary components of the SoC. 
Currently the SoC uses the DDR ip from the FPGA vendor and is thus not 
available for simulation through open source simulators like verilator. 

In this case, we replace the DDR controller slave with a simple RAM based
memory in the test-bench to enable simulation.

Verilator Executable
--------------------

We will be using the open source verilator tool to build a simulation executable. Please ensure
you have the latest version of verilator installed or atleast version 4.08+.

The YAML for simulation configuration is : chromiteh_sim.yaml.
Once you have changed the settings the chromiteh_sim.yaml use the following steps to generate a
verilated executable:


.. code-block:: shell-session

   $ python -m configure.main -ispec chromiteh_sim.yaml
   $ make generate_verilog -j<jobs>
   $ make link_verilator

The above should generate a ``bin`` folder with the following file:

- ``chromiteh_soc``: verilated executable

Memory Hex Files
----------------

As described in :ref:`memory_map`, there are three memory sources in the SoC : 

  - 256MB of DDR
  - 4KB Boot ROM
  - 16KB of OCM (On Chip Memory). 

By default the ZSBL (Zeroth Stage Boot Loader) will print the incore ascii art along with the build information
and either execute an `ebreak` or jump to a different region depending on the boot config provided
during simulation. The ZSBL execution can be skipped in simulation by changing the ``reset_pc`` 
parameter in the chromiteh_sim.yaml to point to any of the above three choices.

The simulation also requires the hex memory files for the ROM, RAM memory and OCM memories.
Defaults copies of these can be generated using the following single command:

.. code-block:: shell-session

   $ make generate_hexfiles

You should now have the following files in the ``bin`` folder:

- ``chromiteh_soc``: verilator executable of the SoC
- ``boot.mem``: hex file containing the ZSBL of Boot ROM
- ``ddr.mem``: hex file for the RAM memory containing a hello-world program
- ``ocm.mem``: hex file for the OCM memory containing a hello-world program

.. note:: The following describes how to generate and modify each of the memory files. These steps can be 
  skipped.

Custom Memory files
^^^^^^^^^^^^^^^^^^^

The boot ROM files can be modified  from the ``boot-code`` folder. The hex files can be generated 
using the following command:

.. code-block:: shell-session
    
  $ cd boot-code; make; cd ..; 
  $ cp boot-code/outputs/boot.mem bin/

This should generate a single file : ``boot.mem`` in the ``bin`` folder

To generate the hex file for the RAM memory perform the following steps

.. code-block:: shell-session

   $ cd software; make PROGRAM=hello ddrhex; cd ..;
   $ cp software/build/ddr.mem bin/

You can change hello to dhrystone or other programs available in ``software/programs`` directory.

Similarly to generate the hex files for the OCM memory do the following:

.. code-block:: shell-session

   $ cd software; make PROGRAM=hello ocmhex; cd ..;
   $ cp software/build/ocm.mem bin/

You can change hello to dhrystone or other programs available in ``software/programs`` directory.

You can start simulation using the ``chromiteh_soc`` executable as elaborated in the next section.

Simulating Executable
---------------------

Once you have the chromiteh_soc executable ready from the above steps, you can use the following
steps to simulate that executable:

- ``./chromiteh_soc``: This will start simulating the core with reset_pc as defined in the yaml. An
  printf statements used in the application will be available in the app_log file. 
- ``./chromiteh_soc +openocd``: With this option the executable will wait for a openocd and gdb
  connection. One can use the scripts avaialable in the ``sim/gdb_setup`` folder.
- ``./chromiteh_soc +boot0``: This option will first execute the ZSBL and exectute an ebreak and
  halt. This is default behavior if no boot option provided.
- ``./chromiteh_soc +boot1``: This option will first execute the ZSBL and jump to OCM region.
- ``./chromiteh_soc +boot2``: This option will first execute the ZSBL and jump to DDR region.

Along with the above, you can also use the following options:

- ``+rtldump``: This will create a instruction trace dump of the execution in ``rtl.dump`` file.


