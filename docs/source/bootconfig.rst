.. _bootconfig:

###########
Boot Config
###########

The Boot Config module implements a single a register which is used by the 
zeroth stage boot loader (ZSBL) which is contained in the Boot Rom. 
Towards the completion of the ZSBL, depending on the value of the boot-config
register the following actions are taken:

.. tabularcolumns:: |l|L|

.. table:: Boot Configuration Options.

  ====== =========================================
  Value  Action
  ====== =========================================
  0      After ZSBL execute an ebreak and allow debugger control of the SoC
  1      After ZSBL jump to OCM
  2      After ZSBL jump to DDR (Includes copying contents from Flash to DDR)
  ====== =========================================

The boot config register values are read-only from the SoC side and are driven 
by external pins, the details of which should be available in the pin
definition of the board.
