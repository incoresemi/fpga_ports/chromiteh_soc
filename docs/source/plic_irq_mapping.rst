
PLIC Interrupt Mapping
======================

.. tabularcolumns:: |l|l|l|

.. _PLIC_interrupt_mapping:

.. table:: PLIC interrupt mapping from devices

   ============ =========== =========================================
   Interrupt ID Peripheral  Description
   ============ =========== =========================================
   1            GPIO-0      GPIO peripheral Interrupt from pin0
   2            GPIO-1      GPIO peripheral Interrupt from pin1
   3            GPIO-2      GPIO peripheral Interrupt from pin2
   4            GPIO-3      GPIO peripheral Interrupt from pin3
   5            GPIO-4      GPIO peripheral Interrupt from pin4
   6            GPIO-5      GPIO peripheral Interrupt from pin5
   7            GPIO-6      GPIO peripheral Interrupt from pin6
   8            GPIO-7      GPIO peripheral Interrupt from pin7
   9            GPIO-8      GPIO peripheral Interrupt from pin8
   10           GPIO-9      GPIO peripheral Interrupt from pin9
   11           GPIO-10     GPIO peripheral Interrupt from pin10
   12           GPIO-11     GPIO peripheral Interrupt from pin11
   13           GPIO-12     GPIO peripheral Interrupt from pin12
   14           GPIO-13     GPIO peripheral Interrupt from pin13
   15           GPIO-14     GPIO peripheral Interrupt from pin14
   16           GPIO-15     GPIO peripheral Interrupt from pin15
   17           UART-0      UART peripheral Interrupt.
   18           PWM0-0      PWM0 peripheral Interrupt from channel 0
   19           PWM0-1      PWM0 peripheral Interrupt from channel 1
   20           PWM0-2      PWM0 peripheral Interrupt from channel 2
   21           PWM0-3      PWM0 peripheral Interrupt from channel 3
   22           PWM0-4      PWM0 peripheral Interrupt from channel 4
   23           PWM0-5      PWM0 peripheral Interrupt from channel 5
   24           SPI -0      SPI0 peripheral Interrupt
   25           SPI -1      SPI1 peripheral Interrupt
   26           SPI -2      SPI2 peripheral Interrupt
   27           QSPI0       Timeout flag interrupt
   28           QSPI0       Status Match Flag
   29           QSPI0       FIFO Threshold Flag
   30           QSPI0       Transfer Complete Flag
   31           QSPI0       Transfer Error Flag
   32           QSPI0       Request Ready Flag
   33           ETHERNET    Ethernet Peripheral Interrupt
   ============ =========== =========================================


