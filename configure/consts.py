# See LICENSE.incore for license details

length_check_fields=['reset_pc', 'physical_addr_size']

bsc_cmd = '''bsc -u -verilog -elab -vdir {0} -bdir {1} -info-dir {1} \
+RTS -K40000M -RTS -check-assert  -keep-fires \
-opt-undetermined-vals -remove-false-rules -remove-empty-rules \
-remove-starved-rules -remove-dollar -unspecified-to X -show-schedule \
-show-module-use {2}'''

bsc_defines = ''

verilator_cmd = ''' -O3 -LDFLAGS "-static" --x-assign fast \
 --x-initial fast --noassert sim_main.cpp --bbox-sys -Wno-STMTDLY \
 -Wno-UNOPTFLAT -Wno-WIDTH -Wno-lint -Wno-COMBDLY -Wno-INITIALDLY \
 --autoflush {0} {1} --threads {2} -DBSV_RESET_FIFO_HEAD \
 -DBSV_RESET_FIFO_ARRAY --output-split 20000 \
 --output-split-ctrace 10000'''

makefile_temp='''
VERILOGDIR:={0}

BSVBUILDDIR:={1}

FPGA_BUILD_DIR:={20}

BSVOUTDIR:={2}

BSCCMD:={3}

BSC_DEFINES=:{4}

BSVINCDIR:={5}

BS_VERILOG_LIB:={6}lib/Verilog/

TOP_MODULE:={7}

TOP_DIR:={8}

TOP_FILE:={9}

VERILATOR_FLAGS:={10}

VERILATOR_SPEED:={11}

XLEN:={12}

TOP_BIN={13}

ISA={14}

FPGA_PART={15}

FPGA_BOARD={16}

FPGA_BOARD_ALIAS={17}

JTAG_TYPE={18}

JOBS={19}

FLASH_BASE_ADDR={21}

FLASH_MAX_ADDR={22}

include depends.mk
'''

dependency_yaml="""\
chromite:
  url: https://gitlab.com/incoresemi/core-generators/chromite.git
  checkout: 0.9.5
  sparse:
    - src/
cache_subsystem:
  url: https://gitlab.com/incoresemi/blocks/cache_subsystem.git
  checkout: fix-sb-busy-use
  sparse :
    - src/icache/
    - src/dcache/
    - src/pmp
    - src/tlbs
common_bsv:
  url: https://gitlab.com/incoresemi/blocks/common_bsv.git
  checkout: master
fabrics:
  url: https://gitlab.com/incoresemi/blocks/fabrics.git
  checkout: 1.1.5
bsvwrappers:
  url: https://gitlab.com/incoresemi/blocks/bsvwrappers.git
  checkout: master
devices:
  url: https://gitlab.com/incoresemi/blocks/devices.git
  checkout: 2.0.0
  sparse:
    - uart/
    - clint/
    - plic/
    - bootconfig/
    - gpio/
    - ram2rw/
    - ram1rw/
    - rom/
    - riscv_debug/
    - jtagdtm/
    - pwm/
    - spi/
    - qspi/
"""

fpga_lookup= {
    'arty100t':{
        'board':'''digilentinc.com:arty-a7-100:part0:1.0''',
        'part':'''xc7a100tcsg324-1''',
        'flash_base_addr':'''4194304''', #0x00400000
        'flash_max_addr':'''16777215'''  #0x00FFFFFF #16MB / 128Mb
    }
}
